<?php

namespace Database\Seeders;

use App\Services\Employer\Employer;
use App\Services\User\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employers = [
            [
                'phone' => '79999999999',
                'email' => 'admin@1creator.ru',
                'first_name' => 'Суперадмин',
                'last_name' => 'Суперадмин',
                'password' => \Illuminate\Support\Facades\Hash::make('password'),
            ],
        ];

        $users = [
            [
                'phone' => '79999999999',
                'email' => 'user@1creator.ru',
                'first_name' => 'Пользователь',
                'last_name' => 'Для тестирования',
                'password' => \Illuminate\Support\Facades\Hash::make('password'),
            ],
        ];

        foreach ($employers as $employer) {
            Employer::create($employer);
        }

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
