import Vue from "vue";
import store from "@/user/js/store/store";
import "@/user/js/directives/vMask";
import App from "@/user/js/components/App";
import router from "@/user/js/router";
import {Multiselect} from "vue-multiselect";
import moment from "moment";

Vue.component("vue-multiselect", Multiselect);

window.app = new Vue({
    el: "#app",
    store: store,
    router: router,
    components: {},
    render: h => h(App)
});

moment.locale("ru");
