<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Главная</title>
    <link href="{{ mix('static/css/vendor.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ mix('static/css/user.css') }}" rel="stylesheet" type="text/css">
    <link href="https://file.myfontastic.com/P8Y4jwtesfg6PpcQGMnxm8/icons.css" rel="stylesheet">

    <style>
        code {
            display: block;
            background: #ececec;
            margin-bottom: 30px;
            padding: 5px;
        }
    </style>
</head>
<body>
<div class="d-flex h-100 align-items-center justify-content-center">
    <div>
        <code>
            php artisan migrate --seed <br/>
            php artisan storage:link
        </code>

        <div class="mb-5">
            <h4>Панель управления</h4>
            <a href="/admin">{{ url('/admin') }}</a>
            <div>admin@1creator.ru / password</div>
        </div>
        <div>
            <h4>Личный кабинет</h4>
            <a href="/user">{{ url('/user') }}</a>
            <div>user@1creator.ru / password</div>
        </div>
    </div>
</div>
<script src="{{ mix('static/js/user.js') }}"></script>
</body>
</html>
