import Vue from "vue";
import VueRouter from "vue-router";
import TheCatalog from "@/admin/js/components/catalog/TheCatalog";
import TheAuth from "@/admin/js/components/auth/TheAuth";
import TheOrders from "@/admin/js/components/orders/TheOrders";
import TheEmployers from "@/admin/js/components/employers/TheEmployers";
import TheAgents from "@/admin/js/components/agents/TheAgents";
import TheUsers from "@/admin/js/components/users/TheUsers";
import TheStatuses from "@/admin/js/components/statuses/TheStatuses";
import TheProfile from "@/admin/js/components/profile/TheProfile";
import store from "@/admin/js/store/store";
import TheGoodEditor from "@/admin/js/components/catalog/TheGoodEditor";
import TheEmployerEditor from "@/admin/js/components/employers/TheEmployerEditor";
import TheAgentEditor from "@/admin/js/components/agents/TheAgentEditor";
import TheUserEditor from "@/admin/js/components/users/TheUserEditor";

Vue.use(VueRouter);

export const routes = [
    {
        path: "/",
        redirect: "/goods",
    },

    {
        path: "/goods",
        component: TheCatalog,
        name: "goods"
    },
    {
        path: "/goods/create",
        component: TheGoodEditor,
        name: "goods.create"
    },
    {
        path: "/goods/:goodId/edit",
        component: TheGoodEditor,
        name: "goods.edit",
        props: true,
    },

    {
        path: "/orders",
        component: TheOrders,
        name: "orders"
    },

    {
        path: "/employers",
        component: TheEmployers,
        name: "employers"
    },
    {
        path: "/employers/create",
        component: TheEmployerEditor,
        name: "employers.create"
    },
    {
        path: "/employers/:employerId/edit",
        component: TheEmployerEditor,
        name: "employers.edit",
        props: true,
    },

    {
        path: "/agents",
        component: TheAgents,
        name: "agents"
    },
    {
        path: "/agents/create",
        component: TheAgentEditor,
        name: "agents.create"
    },
    {
        path: "/agents/:agentId/edit",
        component: TheAgentEditor,
        name: "agents.edit",
        props: true,
    },

    {
        path: "/users",
        component: TheUsers,
        name: "users"
    },
    {
        path: "/users/create",
        component: TheUserEditor,
        name: "users.create"
    },
    {
        path: "/users/:userId/edit",
        component: TheUserEditor,
        name: "users.edit",
        props: true,
    },

    {
        path: "/statuses",
        component: TheStatuses,
        name: "statuses"
    },

    {
        path: "/profile",
        component: TheProfile,
        name: "profile"
    },

    {
        path: "/login",
        component: TheAuth,
        name: "login"
    },
];

const router = new VueRouter({
    mode: "history",
    routes: routes,
    base: "admin",
});

router.beforeEach((to, from, next) => {
    if (to.name != "login" && !store.getters["auth/authorized"]) {
        router.push({name: "login"});
    }
    store.commit("sidebar/toggle", false);
    next()
});

export default router;
