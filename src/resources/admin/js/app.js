import Vue from "vue";
import store from "@/admin/js/store/store";
import "@/user/js/directives/vMask";
import App from "@/admin/js/components/App";
import router from "@/admin/js/router";
import {Multiselect} from "vue-multiselect";
import moment from 'moment';

Vue.component('vue-multiselect', Multiselect);

window.app = new Vue({
    el: "#app",
    store: store,
    router: router,
    components: {},
    render: h => h(App)
});

moment.locale('ru');
