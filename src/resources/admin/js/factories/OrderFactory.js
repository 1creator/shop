/**
 * @typedef {Object} Order
 * @property {number} id
 * @property {number} employerId
 * @property {number} agentId
 * @property {number} userId
 * @property {number} statusId
 * @property {object[]} items
 * @property {string} comment
 * @property {string} deliveryAddress
 */

export default {
    /**
     * @returns {Order}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                employerId: null,
                agentId: null,
                userId: null,
                statusId: null,
                comment: null,
                deliveryAddress: null,
                items: [],
                createdAt: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
