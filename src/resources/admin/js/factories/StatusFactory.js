/**
 * @typedef {Object} Status
 * @property {number} id
 * @property {string} name
 * @property {string} background
 * @property {string} color
 * @property {"order"|"order_item"} type
 */

export default {
    /**
     * @returns {Status}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                name: null,
                type: "order",
                background: "#e5e5e5",
                color: "#373737",
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
