/**
 * @typedef {Object} User
 * @property {number} id
 * @property {string} firstName
 * @property {string} lastName
 * @property {number} email
 * @property {number} phone
 * @property {number} password
 * @property {boolean} active
 * @property {Object} passwordConfirmation
 * @property {string} createdAt
 */

export default {
    /**
     * @returns {User}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                firstName: null,
                lastName: null,
                email: null,
                phone: null,
                active: true,
                password: null,
                passwordConfirmation: null,
                createdAt: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
