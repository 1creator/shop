/**
 * @typedef {Object} Agent
 * @property {number} id
 * @property {string} name
 * @property {string} active
 * @property {string} createdAt
 */

export default {
    /**
     * @returns {Agent}
     */
    create(props = {}) {
        return {
            ...{
                id: null,
                name: null,
                active: true,
                createdAt: null,
            },
            ...JSON.parse(JSON.stringify(props)),
        };
    }

}
