import {statuses} from "@/admin/js/api";

const state = {
    items: window.initialState.statuses || []
};

const getters = {
    findById: state => id => state.items.find(item => item.id == id),
    findByIdSafe: state => id => {
        return state.items.find(item => item.id == id) || {
            id: null,
            name: "Статус не определен",
            color: "#545454",
            background: "#ffdfdf",
        };
    },
    order: state => state.items.filter(item => item.type === "order"),
    orderItem: state => state.items.filter(item => item.type === "order_item"),
};

const actions = {
    async store(context, data) {
        const res = await statuses.store(data);
        context.commit("add", res);
    },
    async update(context, {id, data}) {
        const res = await statuses.update(id, data);
        context.commit("update", {id: id, data: res});
    },
    async delete(context, id) {
        const res = await statuses.delete(id);
        context.commit("remove", id);
    },
};

const mutations = {
    setItems(state, items) {
        state.items = items;
    },
    add(state, item) {
        state.items.push(item);
    },
    update(state, {id, data}) {
        let updatingItem = state.items.find(item => item.id == id);
        if (updatingItem) {
            Object.assign(updatingItem, data);
        }
    },
    remove(state, id) {
        let index = state.items.findIndex(item => item.id == id);
        if (index >= 0) {
            state.items.splice(index, 1);
        }
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
