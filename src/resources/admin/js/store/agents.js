import {agents} from "@/admin/js/api";

const state = {
    items: window.initialState.agents || []
};

const getters = {
    findById: state => id => state.items.find(item => item.id == id),
};

const actions = {
    async store(context, data) {
        const res = await agents.store(data);
        context.commit("add", res);
    },
    async update(context, {id, data}) {
        const res = await agents.update(id, data);
        context.commit("update", {id: id, data: res});
    },
    async delete(context, id) {
        const res = await agents.delete(id);
        context.commit("remove", id);
    },
};

const mutations = {
    setItems(state, items) {
        state.items = items;
    },
    add(state, item) {
        state.items.push(item);
    },
    update(state, {id, data}) {
        let updatingItem = state.items.find(item => item.id == id);
        if (updatingItem) {
            Object.assign(updatingItem, data);
        }
    },
    remove(state, id) {
        let index = state.items.findIndex(item => item.id == id);
        if (index >= 0) {
            state.items.splice(index, 1);
        }
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
