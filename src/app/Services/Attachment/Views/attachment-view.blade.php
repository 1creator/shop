@if($attachment->type == 'embed')
    <div {{ $attributes }}>
        {!! $attachment->url !!}
    </div>
@else
    <img src="{{ $attachment->url }}" alt="{{ $attachment->alt }}" {{ $attributes }}/>
@endif
