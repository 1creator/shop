<?php


namespace App\Services\Attachment\Services;


use App\Services\Attachment\Interfaces\AttachmentInterface;
use App\Services\Attachment\Models\Attachment;
use App\Services\Attachment\Resources\AttachmentResource;
use Illuminate\Support\Facades\Storage;
use Spatie\ImageOptimizer\OptimizerChain;

class AttachmentService implements AttachmentInterface
{

    public function store($file, $type)
    {
        $createdFile = null;
        if ($type == 'embed')
            $createdFile = $this->storeIframe($file);
        else {
            $createdFile = $this->storeFile($file);
        }
        return new AttachmentResource($createdFile);
    }

    public function storeFile($file)
    {
        $disk = config('attachments.disk', 'public');
        app(OptimizerChain::class)->optimize($file);
        $path = Storage::disk($disk)->putFile('', $file);

        $name = method_exists($file, 'getClientOriginalName') ?
            $file->getClientOriginalName() : $file->getFilename();
        return Attachment::create([
            'name' => $name,
            'type' => $file->getMimeType(),
            'size' => $file->getSize(),
            'original' => $path,
            'disk' => $disk,
//            'thumbnail' => $thumbnailUrl ?? null,
        ]);
    }

    public function storeIframe($file)
    {
        return Attachment::create([
            'name' => 'Внешнее содержимое',
            'type' => 'embed',
            'original' => $file,
            'thumbnail' => null,
        ]);
    }
}
