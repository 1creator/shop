<?php


namespace App\Services\Shop;


use App\Services\Agent\Agent;
use App\Services\Employer\Employer;
use App\Services\User\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];
//    protected $with = ['status'];

//    public function orders()
//    {
//        return $this->hasMany(Order::class);
//    }
//
//    public function lastOrder()
//    {
//        return $this->hasOne(Order::class)->latest();
//    }

    public function status()
    {
        return $this->belongsTo(Status::class)->withDefault([
            'name' => 'Статус не определен',
        ]);
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class)->withDefault([
            'name' => 'Сотрудник не указан',
        ]);
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class)->withDefault([
            'name' => 'Агент не указан',
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }
}
