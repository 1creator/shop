<?php


namespace App\Services\Shop;


use Exception;
use Illuminate\Support\Facades\Validator;

class StatusService
{
    public function store(array $data): Status
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'name' => 'required|string|max:100',
            'type' => 'required|in:order,order_item',
            'color' => 'required|string|max:20',
            'background' => 'required|string|max:20',
        ]);

        $status = new Status($data->only([
            'name', 'type', 'background', 'color'
        ])->toArray());

        $status->save();

        return $status;
    }

    public function update(Status $status, array $data): Status
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'name' => 'required|string|max:100',
            'type' => 'required|in:order,order_item',
            'color' => 'required|string|max:20',
            'background' => 'required|string|max:20',
        ]);

        $status->update($data->only([
            'name', 'type', 'background', 'color'
        ])->toArray());

        return $status;
    }

    public function destroy(Status $status): bool
    {
        try {
            return $status->delete() ? 1 : 0;
        } catch (Exception $e) {
            return 0;
        }
    }
}
