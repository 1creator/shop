<?php


namespace App\Services\Shop;


use App\Services\Attachment\Models\Attachment;
use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    protected $guarded = ['id'];
    protected $casts = [
        'price_rub' => 'float',
    ];
    protected $with = ['image'];

//    public function orders()
//    {
//        return $this->hasMany(Order::class);
//    }
//
//    public function lastOrder()
//    {
//        return $this->hasOne(Order::class)->latest();
//    }

    public function image()
    {
        return $this->morphOne(Attachment::class, 'attachable');
    }
}
