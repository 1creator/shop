<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->foreignId('employer_id')->nullable()
                ->references('id')->on('employers')
                ->nullOnDelete()->cascadeOnUpdate();

            $table->foreignId('user_id')->nullable()
                ->references('id')->on('users')
                ->nullOnDelete()->cascadeOnUpdate();

            $table->foreignId('agent_id')->nullable()
                ->references('id')->on('agents')
                ->nullOnDelete()->cascadeOnUpdate();

            $table->foreignId('status_id')->nullable()
                ->references('id')->on('statuses')
                ->nullOnDelete()->cascadeOnUpdate();

            $table->text('comment')->nullable();

            $table->text('delivery_address')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
