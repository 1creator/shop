<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();

            $table->foreignId('order_id')
                ->references('id')->on('orders')
                ->cascadeOnDelete()->cascadeOnUpdate();

            $table->foreignId('good_id')->nullable()
                ->references('id')->on('goods')
                ->nullOnDelete()->cascadeOnUpdate();

            $table->foreignId('status_id')->nullable()
                ->references('id')->on('statuses')
                ->nullOnDelete()->cascadeOnUpdate();

            $table->unsignedInteger('count')->default(1);

            $table->decimal('price_rub', 10, 2, true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
