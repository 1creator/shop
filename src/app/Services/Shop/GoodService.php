<?php


namespace App\Services\Shop;


use App\Services\Attachment\Models\Attachment;
use Exception;
use Illuminate\Support\Facades\Validator;

class GoodService
{
    public function store(array $data): Good
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'name' => 'required|string|max:100',
            'description' => 'nullable|string',
            'active' => 'boolean',
            'price_rub' => 'required|numeric|min:0',
        ]);

        $good = new Good($data->only([
            'description', 'name', 'active', 'price_rub'
        ])->toArray());

        $good->save();

        if ($data->has('image_id')) {
            if ($data['image_id']) {
                $attachment = Attachment::findOrFail($data['image_id']);
                $good->image()->save($attachment);
            }
            $good->load('image');
        }

        return $good;
    }

    public function update(Good $good, array $data): Good
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'name' => 'required|string|max:100',
            'description' => 'nullable|string',
            'active' => 'boolean',
            'price_rub' => 'required|numeric|min:0',
        ]);

        $good->update($data->only([
            'description', 'name', 'active', 'price_rub'
        ])->toArray());

        if ($data->has('image_id') && $data['image_id'] != ($good->image->id ?? null)) {
            $good->image()->delete();
            if ($data['image_id']) {
                $attachment = Attachment::findOrFail($data['image_id']);
                $good->image()->save($attachment);
            }
            $good->load('image');
        }

        return $good;
    }

    public function destroy(Good $good): bool
    {
        try {
            return $good->delete() ? 1 : 0;
        } catch (Exception $e) {
            return 0;
        }
    }
}
