<?php


namespace App\Services\Shop;


use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $guarded = ['id'];
    protected $with = ['good'];
    protected $casts = [
        'price_rub' => 'float',
    ];

//    public function orders()
//    {
//        return $this->hasMany(Order::class);
//    }
//
//    public function lastOrder()
//    {
//        return $this->hasOne(Order::class)->latest();
//    }
    public function status()
    {
        return $this->belongsTo(Status::class)->withDefault([
            'name' => 'Статус не определен',
        ]);
    }

    public function good()
    {
        return $this->belongsTo(Good::class);
    }

}
