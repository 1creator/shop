<?php


namespace App\Services\Shop;


use Exception;
use Illuminate\Support\Facades\Validator;

class OrderService
{
    public function store(array $data): Order
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'agent_id' => 'nullable|exists:agents,id',
            'employer_id' => 'nullable|exists:employers,id',
            'user_id' => 'nullable|exists:users,id',
            'status_id' => 'nullable|exists:statuses,id',
            'delivery_address' => 'nullable|string|max:255',
            'comment' => 'nullable|string|max:255',
            'items' => 'array|required|min:1'
        ]);

        $order = new Order($data->only([
            'name', 'type', 'agent_id', 'employer_id', 'user_id', 'status_id', 'delivery_address', 'comment'
        ])->toArray());
        $order->save();

        foreach ($data['items'] as $item) {
            $good = Good::findOrFail($item['good_id']);
            $orderItem = new OrderItem(collect($item)->only([
                'good_id', 'count', 'price_rub', 'status_id'
            ])->toArray());
            if (!$orderItem->price_rub){
                $orderItem->price_rub = $good->price_rub;
            }
            $order->items()->save($orderItem);
        }

        $order->load(['items', 'user', 'employer', 'agent']);

        return $order;
    }

    public function update(Order $order, array $data): Order
    {
        $data = collect($data);
        Validator::validate($data->toArray(), [
            'agent_id' => 'nullable|exists:agents,id',
            'employer_id' => 'nullable|exists:employers,id',
            'user_id' => 'nullable|exists:users,id',
            'status_id' => 'nullable|exists:statuses,id',
            'delivery_address' => 'nullable|string|max:255',
            'comment' => 'nullable|string|max:255',
            'items' => 'array|required|min:1'
        ]);

        $order->update($data->only([
            'name', 'type', 'agent_id', 'employer_id', 'user_id', 'status_id', 'delivery_address', 'comment'
        ])->toArray());

        if ($data->has('items')) {
            $itemIds = [];
            foreach ($data['items'] as $item) {
                $itemModel = null;
                $id = $item['id'] ?? null;
                if ($id) {
                    $itemModel = $order->items->where('id', $id)->first();
                }
                if ($itemModel) {
                    $itemModel['good_id'] = $item['good_id'];
                    $itemModel['count'] = $item['count'];
                    $itemModel['price_rub'] = $item['price_rub'];
                    $itemModel['status_id'] = $item['status_id'];
                    $itemModel->save();
                } else {
                    $good = Good::findOrFail($item['good_id']);
                    $itemModel = new OrderItem([
                        'order_id' => $order['id'],
                        'good_id' => $item['good_id'],
                        'count' => $item['count'],
                        'price_rub' => $item['price_rub'] ?? $good->price_rub,
                        'status_id' => $item['status_id'],
                    ]);
                    $itemModel->save();
                }
                array_push($itemIds, $itemModel['id']);
            }
            $order->items()->whereNotIn('id', $itemIds)->delete();
        }

        $order->load(['items', 'user', 'employer', 'agent']);

        return $order;
    }

    public function destroy(Order $order): bool
    {
        try {
            return $order->delete() ? 1 : 0;
        } catch (Exception $e) {
            return 0;
        }
    }
}
