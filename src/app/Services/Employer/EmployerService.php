<?php


namespace App\Services\Employer;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EmployerService
{
    public function store(array $data)
    {
        Validator::validate($data, [
            'first_name' => 'required|string|max:100',
            'last_name' => 'string|max:100',
            'phone' => 'nullable|string|max:20|unique:employers,phone',
            'has_access' => 'boolean',
            'email' => 'required_if:has_access,true|nullable|email|unique:employers,email',
            'password' => 'required_if:has_access,true|nullable|string|min:4|confirmed',
        ]);

        $data = collect($data);
        $employer = new Employer();
        $employer->fill($data->only(['first_name', 'last_name', 'phone', 'has_access', 'email', 'position'])->toArray());
        $employer->password = Hash::make($data['password']);
        $employer->save();

        return $employer;
    }

    public function update(Employer $employer, array $data)
    {
        Validator::validate($data, [
            'first_name' => 'sometimes|required|string|max:100',
            'last_name' => 'sometimes|string|max:100',
            'email' => 'sometimes|required|email|unique:employers,email,' . $employer->id,
            'phone' => 'nullable|string|max:20|unique:employers,phone,' . $employer->id,
            'has_access' => 'sometimes|boolean',
            'password' => 'sometimes|nullable|confirmed|min:4|string',
        ]);

        $data = collect($data);
        $employer->fill($data->only(['first_name', 'last_name', 'phone', 'has_access', 'email', 'position'])->toArray());
        if ($data['password'] ?? null) {
            $employer->password = Hash::make($data['password']);
        }
        $employer->save();

        return $employer;
    }
}
