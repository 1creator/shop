<?php


namespace App\Services\User;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;

    protected $hidden = ['password', 'remember_token'];
    protected $guarded = ['id'];

//    public function orders()
//    {
//        return $this->hasMany(Order::class);
//    }
//
//    public function lastOrder()
//    {
//        return $this->hasOne(Order::class)->latest();
//    }
}
