<?php


namespace App\Services\User;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserService
{
    public function store(array $data)
    {
        Validator::validate($data, [
            'first_name' => 'required|string|max:100',
            'last_name' => 'string|max:100',
            'phone' => 'nullable|string|max:20|unique:users,phone',
            'email' => 'required|email|unique:users,email',
            'active' => 'boolean',
            'password' => 'required_if:has_access,true|nullable|string|min:4|confirmed',
        ]);

        $data = collect($data);
        $user = new User($data->only(['first_name', 'last_name', 'email', 'phone', 'active'])->toArray());
        $user->password = Hash::make($data['password']);
        $user->save();

        return $user;
    }

    public function update(User $user, array $data)
    {
        Validator::validate($data, [
            'first_name' => 'sometimes|required|string|max:100',
            'last_name' => 'sometimes|string|max:100',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'phone' => 'nullable|sometimes|string|max:20|unique:users,phone,' . $user->id,
            'active' => 'sometimes|boolean',
            'password' => 'sometimes|nullable|confirmed|min:4|string',
        ]);

        $data = collect($data);
        $user->fill($data->only(['first_name', 'last_name', 'email', 'phone', 'active'])->toArray());
        if ($data['password'] ?? null) {
            $user->password = Hash::make($data['password']);
        }
        $user->save();

        return $user;
    }
}
