<?php


namespace App\Services\Agent;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AgentService
{
    public function store(array $data)
    {
        Validator::validate($data, [
            'name' => 'required|string|max:100',
            'active' => 'boolean',
        ]);

        $data = collect($data);
        $agent = new Agent($data->only(['name', 'active'])->toArray());
        $agent->save();

        return $agent;
    }

    public function update(Agent $agent, array $data)
    {
        Validator::validate($data, [
            'name' => 'required|string|max:100',
            'active' => 'boolean',
        ]);

        $data = collect($data);
        $agent = new Agent($data->only(['name', 'active'])->toArray());
        $agent->save();

        return $agent;
    }
}
