<?php


namespace App\Http\Controllers\User\Api;


use App\Http\Controllers\Controller;
use App\Http\Middleware\TransformRequestPhone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(TransformRequestPhone::class);
    }

    public function index(Request $request)
    {
        return Auth::guard('web')->user();
    }

    public function update(Request $request)
    {
        $user = Auth::guard('web')->user();
        $request->validate([
            'first_name' => 'required|string|max:100',
            'last_name' => 'string|max:100',
            'phone' => 'nullable|string|max:20|unique:employers,phone,' . $user->id,
            'email' => 'required_if:has_access,true|nullable|email|unique:employers,email,' . $user->id,
            'password' => 'sometimes|nullable|string|min:4|confirmed',
        ]);

        $user->fill($request->only([
            'first_name', 'last_name', 'email', 'phone',
        ]));
        if ($request->filled('password')) {
            $user->password = Hash::make($request->input('password'));
        }
        $user->save();
        return $user;
    }
}
