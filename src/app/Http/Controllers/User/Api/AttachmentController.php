<?php


namespace App\Http\Controllers\User\Api;


use App\Http\Controllers\Controller;
use App\Services\Attachment\Models\Attachment;
use App\Services\Attachment\Resources\AttachmentResource;
use App\Services\Attachment\Services\AttachmentService;
use Illuminate\Http\Request;

class AttachmentController extends Controller
{
    public function store(Request $request)
    {
        return app(AttachmentService::class)->store($request->file('file'), $request->type);
    }
}
