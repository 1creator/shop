<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\Shop\Good;
use App\Services\Shop\Status;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $user = Auth::guard('web')->user();
        $statuses = Status::all();
        $goods = Good::where('active', 1)->get();

        $data = collect([
            'user' => $user,
            'statuses' => $statuses,
            'goods' => $goods,
        ]);

        $data = $data->camelCaseKeys();

        return view('user.views.app', $data);
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect('/user/login');
    }
}
