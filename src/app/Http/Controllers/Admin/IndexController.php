<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Agent\Agent;
use App\Services\Employer\Employer;
use App\Services\Shop\Good;
use App\Services\Shop\Status;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $user = Auth::guard('admin')->user();
        $goods = Good::all();
        $employers = Employer::all();
        $agents = Agent::all();
        $statuses = Status::all();

        $data = collect([
            'user' => $user,
            'goods' => $goods,
            'employers' => $employers,
            'agents' => $agents,
            'statuses' => $statuses,
        ]);

        $data = $data->camelCaseKeys();

        return view('admin.views.app', $data);
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin/login');
    }
}
