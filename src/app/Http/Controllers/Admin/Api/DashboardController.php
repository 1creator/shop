<?php

namespace App\Http\Controllers\Admin\Api;

use App\Services\User\Good;

class DashboardController
{
    public function index()
    {
        $goods = Good::all();
        return [
            'goods' => $goods,
        ];
    }
}
