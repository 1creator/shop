<?php


namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Services\Shop\Order;
use App\Services\Shop\OrderService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        return QueryBuilder::for(Order::class)
            ->allowedIncludes(['user', 'items', 'employer'])
            ->defaultSort('-created_at')
            ->get();
    }

    public function store(Request $request)
    {
        return app(OrderService::class)->store($request->all());
    }

    public function update(Request $request, Order $order)
    {
        return app(OrderService::class)->update($order, $request->all());
    }

    public function destroy(Request $request, Order $order)
    {
        return $order->delete() ? 1 : 0;
    }
}
