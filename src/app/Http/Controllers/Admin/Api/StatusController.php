<?php


namespace App\Http\Controllers\Admin\Api;


use App\Http\Controllers\Controller;
use App\Services\Shop\Status;
use App\Services\Shop\StatusService;
use App\Utils\QueryBuilder;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function index(Request $request)
    {
        return QueryBuilder::for(Status::class)->get();
    }

    public function store(Request $request)
    {
        return app(StatusService::class)->store($request->all());
    }

    public function show(Request $request, Status $status)
    {
        return $status;
    }

    public function update(Request $request, Status $status)
    {
        return app(StatusService::class)->update($status, $request->all());
    }

    public function destroy(Request $request, Status $status)
    {
        return app(StatusService::class)->destroy($status);
    }
}
