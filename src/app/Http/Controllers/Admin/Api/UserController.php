<?php


namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Http\Middleware\TransformRequestPhone;
use App\Services\User\User;
use App\Services\User\UserService;
use App\Utils\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(TransformRequestPhone::class);
    }

    public function index(Request $request)
    {
        return QueryBuilder::for(User::class)
            ->allowedFilters(AllowedFilter::callback('query', function (Builder $builder, $val) {
                $builder->where('email', 'LIKE', '%' . $val . '%')
                    ->orWhere('phone', 'LIKE', '%' . $val . '%');
            }))
            ->get();
    }

    public function store(Request $request)
    {
        return app(UserService::class)->store($request->all());
    }

    public function update(Request $request, User $user)
    {
        return app(UserService::class)->update($user, $request->all());
    }

    public function show(Request $request, User $user)
    {
        return $user;
    }

    public function destroy(Request $request, User $user)
    {
        return $user->delete() ? 1 : 0;
    }
}
