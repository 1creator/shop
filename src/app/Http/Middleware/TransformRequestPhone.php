<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class TransformRequestPhone
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->has('phone')) {
            $phone = $request->input('phone');
            $phone = preg_replace('/[^0-9]/', '', $phone);
            if (strlen($phone) == 1) $phone = null;
            $request->merge([
                'phone' => $phone,
            ]);
        }

        return $next($request);
    }
}
