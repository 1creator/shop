<?php

namespace App\Providers;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        JsonResource::withoutWrapping();

        Collection::macro('camelCaseKeys', function () {
            return $this->mapWithKeys(function ($value, $key) {
                if (is_array($value)) {
                    $res = collect($value)->camelCaseKeys();
                } elseif (method_exists($value, 'toArray')) {
                    $res = collect($value->toArray())->camelCaseKeys();
                } else {
                    $res = $value;
                }
                return [Str::camel($key) => $res];
            });
        });
    }
}
