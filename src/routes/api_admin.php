<?php

use App\Http\Controllers\Admin\Api\AgentController;
use App\Http\Controllers\Admin\Api\AuthController;
use App\Http\Controllers\Admin\Api\DashboardController;
use App\Http\Controllers\Admin\Api\EmployerController;
use App\Http\Controllers\Admin\Api\GoodController;
use App\Http\Controllers\Admin\Api\OrderController;
use App\Http\Controllers\Admin\Api\ProfileController;
use App\Http\Controllers\Admin\Api\StatusController;
use App\Http\Controllers\Admin\Api\UserController;
use App\Http\Controllers\User\Api\AttachmentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'admin',
], function () {
    Route::post('login', [AuthController::class, 'login']);

    Route::group([
        'middleware' => ['auth:admin'],
    ], function () {
        Route::get('dashboard', [DashboardController::class, 'index']);
        Route::put('profile', [ProfileController::class, 'update']);

        Route::resources([
            'goods' => GoodController::class,
            'users' => UserController::class,
            'employers' => EmployerController::class,
            'agents' => AgentController::class,
            'statuses' => StatusController::class,
            'orders' => OrderController::class,
        ]);
    });
});
