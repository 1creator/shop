<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\IndexController as UserIndexController;
use App\Http\Controllers\Admin\IndexController as AdminIndexController;
use App\Http\Controllers\Site\IndexController as SiteIndexController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [SiteIndexController::class, 'index']);

Route::get('/admin/logout', [AdminIndexController::class, 'logout']);
Route::get('/admin{any}', [AdminIndexController::class, 'index'])
    ->where('any', '.*');

Route::get('/user/logout', [UserIndexController::class, 'logout']);
Route::get('/user{any}', [UserIndexController::class, 'index'])
    ->where('any', '.*');
