const mix = require("laravel-mix");
const config = require("./webpack.config");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig(config);
mix.disableSuccessNotifications();

mix.options({
    processCssUrls: false,
    output: {
        chunkFilename: "js/chunks/[name].js",
    },
});
mix.version();

mix
    .js("resources/user/js/app.js", "public/static/js/user.js")
    .js("resources/admin/js/app.js", "public/static/js/admin.js")
    .sass("resources/user/sass/vendor.scss", "public/static/css/vendor.css")
    .sass("resources/user/sass/app.scss", "public/static/css/user.css");
//
// mix.browserSync({
//     proxy: "localhost"
// });
